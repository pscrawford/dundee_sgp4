# -- Configurable
CDEFINES=-O2

#CDEFINES=-O2 -DSGDP4_SNGL -DDEBUG
#CDEFINES=-O2 -DMACRO_SINCOS
#CDEFINES=-O2 -DNO_DEEP_SPACE

# -- Linux
CC=gcc
CFLAGS=-Wall -DLINUX $(CDEFINES)
XLIBS=

# Common commands
RM=rm -f
AR=ar cr
RANLIB=ranlib
DEPFLAGS=-I/usr/include

SRCS=aries.c deep.c ferror.c satutl.c sgdp4.c

OBJS=${SRCS:.c=.o}

# Need 'lm for sincos and similar math functions
LIB=-lm $(XLIBS)

# List of compiled binaries
BIN=testsgp compvec

all: $(BIN)

testsgp: $(OBJS) test1.c
	$(CC) $(CFLAGS) $(DEPFLAGS) -o $@ test1.c $(OBJS) $(LIB)

compvec: comp.c
	$(CC) $(CFLAGS) $(DEPFLAGS) -o $@ comp.c $(LIB)

test:
	$(RM) test_?.txt
	./testsgp -i twoline_example.txt -c ssd.txt > test_b.txt
	./compvec test_b.txt test_example.txt 0 > diff.txt
	@echo "Check errors are below 1E-6 (double precision), or average 0.0877 (single)"
	@tail -n 1 diff.txt

clean:
	$(RM) core $(OBJS)

cleanall:
	$(RM) core $(OBJS) *.OBJ *.BAK *.exe *.bak $(BIN) *~ .Makefile test_?.txt diff.txt

tar:
	@make cleanall
	@tar -czf ../dundee-sgp4.tgz *

.c.o:
	$(CC) $(CFLAGS) $(DEPFLAGS) -c $*.c

depend:
	makedepend $(CFLAGS) $(DEPFLAGS) $(SRCS)
	@sed -f nosysdep.sed < Makefile > Makefile.tmp
	@mv Makefile.tmp Makefile
	@mv Makefile.bak .Makefile

# DO NOT DELETE THIS LINE -- make depend depends on it.

aries.o: sgdp4h.h
aries.o: satutl.h
deep.o: sgdp4h.h
deep.o: satutl.h
ferror.o: sgdp4h.h
ferror.o: satutl.h
satutl.o: sgdp4h.h
satutl.o: satutl.h
sgdp4.o: sgdp4h.h
sgdp4.o: satutl.h
