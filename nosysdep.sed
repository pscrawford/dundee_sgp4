# Remove dependencies on system include files
/[^ 	]*\.o:/s/\/usr\/include\/[^ 	]*[ 	]*//g
/[^ 	]*\.o:/s/\/usr\/include\/[^ 	]*$//
/[^ 	]*\.o:/s/\/usr\/local\/lib\/[^ 	]*[ 	]*//g
/[^ 	]*\.o:/s/\/usr\/local\/lib\/[^ 	]*$//
# This one remove my .c.o: line!
#/[^	]*\.o:[ 	]*$/d
/[^	]*\.o: $/d
# Strip trailing whitespace
s/[ \t]*$//
